package minhphu.english.phrasalverb.ui.promoteapp

import android.content.Context
import minhphu.english.phrasalverb.R

class AppPromote(
        var appName: String,
        var appIcon: Int,
        var appUrl: String,
        var isMoreApp: Boolean = false
) {
    companion object {
        fun getListPromoteApp(context: Context): ArrayList<AppPromote> {
            val listMoreApp = ArrayList<AppPromote>()

            listMoreApp.add(
                    AppPromote(
                            "Idioms and Phrases",
                            R.drawable.idiom_256,
                            context.getString(R.string.package_idiom_phrase)
                    )
            )
            listMoreApp.add(
                    AppPromote(
                            "Slang English",
                            R.drawable.slang_256,
                            context.getString(R.string.package_slang)
                    )
            )

            listMoreApp.add(
                    AppPromote(
                            "English Grammar",
                            R.drawable.grammar_icon_256,
                            context.getString(R.string.package_grammar)
                    )
            )

            listMoreApp.add(
                    AppPromote(
                            "Vocabulary Builder",
                            R.drawable.voca_builder_256,
                            context.getString(R.string.package_vocabulary_builder)
                    )
            )

            listMoreApp.add(
                    AppPromote(
                            "More App",
                            R.drawable.icon_dev,
                            context.getString(R.string.dev_page), true
                    )
            )
            return listMoreApp
        }
    }
}