package minhphu.english.phrasalverb.ui

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.net.Uri
import android.os.Bundle
import android.os.IBinder
import android.preference.PreferenceManager
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.RecyclerView
import com.android.vending.billing.IInAppBillingService
import com.facebook.ads.Ad
import com.facebook.ads.AdError
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.google.android.gms.ads.MobileAds
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_main.*
import minhphu.english.phrasalverb.BuildConfig
import minhphu.english.phrasalverb.R
import minhphu.english.phrasalverb.ui.lesson.LessonFragment
import minhphu.english.phrasalverb.ui.promoteapp.AppPromote
import minhphu.english.phrasalverb.ui.promoteapp.AppPromoteAdapter
import minhphu.english.phrasalverb.utils.AdUtil
import minhphu.english.phrasalverb.utils.Constants
import minhphu.english.phrasalverb.utils.Utils
import minhphu.english.phrasalverb.utils.purchase.IabHelper
import minhphu.english.phrasalverb.utils.purchase.IabResult
import minhphu.english.phrasalverb.utils.purchase.Purchase

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener,
    IabHelper.OnIabPurchaseFinishedListener, IabHelper.OnIabSetupFinishedListener,
    NavController.OnDestinationChangedListener, AppPromoteAdapter.OnPromoteClick {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        MobileAds.initialize(this, getString(R.string.app_ads_id))
        restorePurchase()
        initPopupFb()
        setSupportActionBar(toolbar)
        setUpDrawer()
        toolbar.setupWithNavController(getNavHostFragment().navController, drawerMain)
        getNavHostFragment().navController.addOnDestinationChangedListener(this)
        navigationMain.setNavigationItemSelectedListener(this)
    }

    fun setTopTitle(title: String) {
        toolbar.title = title
    }

    companion object {
        // when back to main UI five times -> show popup
        const val COUNT_BACK_TO_MAIN = 4

        // when open app , popup must show two times
        const val COUNT_POPUP_SHOWED = 2

        const val SKU_REMOVE_ADS = "minhphu.english.phrasalverb.removeads"
        const val base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoBKCK+w3DPMVO+0brpCkKgzI/2azoYSOFICPCMFUzHkcqiJjFgKbvnIjEVb57N6wXxjiCkK85DBqLzp3NmCWWhZvX7bMNxYD0SBtbp0uWWyZV0lR9DbTtfwY+FEE5jeiN8QXeedyCG9Rrs51wV+CGW2r2dfLZ1XeH74ejioihp4jFcEWY+elIu5fZAdzi1tLLorG+FYbDxW/PgwBJeosgSvbKhksrFeG0jSNQRaGayuQxrKvNqYtBP19VmeW1wi1HZ3MuGay2oYt5z9j1pFgbEk0jy46uVu3fKZCEsFgO+gYUAJ4dWs3yhBdKqiXhuagL/kQZuUM6fint4m8Cw80GwIDAQAB"
    }

    private var mInterstitialAdGg: InterstitialAd? = null
    private var mInterstitialAdFb: com.facebook.ads.InterstitialAd? = null
    private var countBackToMain = 0
    private var numberPopupShowed = 0

    override fun onDestinationChanged(
        controller: NavController,
        destination: NavDestination,
        arguments: Bundle?
    ) {
        when (destination.id) {
            R.id.lessonFragment -> {
                if (numberPopupShowed < COUNT_POPUP_SHOWED) {
                    countBackToMain++
                    if (countBackToMain >= COUNT_BACK_TO_MAIN) {
                        showInterstitialAds()
                    }
                }
            }
        }
    }

    private fun getNavHostFragment(): NavHostFragment = supportFragmentManager
        .findFragmentById(R.id.navigationFragment) as NavHostFragment

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_remove_ads -> {
                if (Utils.isNetworkConnected(this)) {
                    startPurchase()
                }
            }
            R.id.action_share_app -> {
                val shareBody = "https://play.google.com/store/apps/details?id=$packageName"
                val sharingIntent = Intent(Intent.ACTION_SEND)
                sharingIntent.type = "text/plain"
                sharingIntent.putExtra(
                    android.content.Intent.EXTRA_SUBJECT,
                    getString(R.string.app_name)
                )
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody)
                startActivity(Intent.createChooser(sharingIntent, getString(R.string.app_name)))
            }
            R.id.action_rate -> openRate()
            R.id.action_data_policy -> Utils.openWebPage(this, getString(R.string.txt_url_policy))
        }
        return true
    }


    private fun setUpDrawer() {
        val toggle = ActionBarDrawerToggle(
            this,
            drawerMain,
            toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawerMain.addDrawerListener(toggle)
        toggle.syncState()

        val v = navigationMain.inflateHeaderView(R.layout.more_app)
        val rcvMoreApp = v.findViewById<RecyclerView>(R.id.rcvMoreApp)
        val adapter = AppPromoteAdapter(
            AppPromote.getListPromoteApp(this@MainActivity), this
        )
        rcvMoreApp.adapter = adapter
        navigationMain.setNavigationItemSelectedListener(this)
    }

    override fun onItemPromoteClick(item: AppPromote) {
        if (Utils.isNetworkConnected(this@MainActivity)) {
            when {
                item.isMoreApp -> {
                    val devIntent =
                        Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.dev_page)))
                    devIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_MULTIPLE_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(devIntent)
                }
                else -> openPromo(item.appUrl)
            }
        } else {
            Toast.makeText(
                this@MainActivity, getString(R.string.txt_check_internet), Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun openPromo(url: String) {
        val marketIntent = Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$url"))
        marketIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_MULTIPLE_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(marketIntent)
    }

    private fun openRate() {
        if (Utils.isNetworkConnected(this)) {
            try {
                val marketIntent =
                    Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$packageName"))
                marketIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_MULTIPLE_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(marketIntent)
            } catch (e: Exception) {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://play.google.com/store/apps/details?id=$packageName")
                    )
                )
            }
        } else {
            Toast.makeText(
                this@MainActivity,
                getString(R.string.txt_check_internet),
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun initPopupAdGg() {
        mInterstitialAdGg = InterstitialAd(this)
        mInterstitialAdGg?.adUnitId = getString(R.string.adsGg_Popup)
        if (!mInterstitialAdGg!!.isLoading && !mInterstitialAdGg!!.isLoaded) {
            val adRequest = AdRequest.Builder().build()
            mInterstitialAdGg?.loadAd(adRequest)
        }
        mInterstitialAdGg?.adListener = object : AdListener() {
            override fun onAdClosed() {
                if (!mInterstitialAdGg!!.isLoading && !mInterstitialAdGg!!.isLoaded) {
                    val adRequest = AdRequest.Builder().build()
                    mInterstitialAdGg?.loadAd(adRequest)
                }
            }
        }
    }

    private fun initPopupFb() {
        mInterstitialAdFb = if (BuildConfig.DEBUG) {
            com.facebook.ads.InterstitialAd(this, AdUtil.ID_TEST_FACEBOOK_ADS)
        } else {
            com.facebook.ads.InterstitialAd(this, getString(R.string.adsFb_Popup))
        }
        mInterstitialAdFb?.setAdListener(object : com.facebook.ads.InterstitialAdListener {
            override fun onAdClicked(p0: Ad?) {}

            override fun onAdLoaded(p0: Ad?) {}

            override fun onLoggingImpression(p0: Ad?) {}

            override fun onInterstitialDisplayed(ad: Ad) {}

            override fun onInterstitialDismissed(ad: Ad) {
                mInterstitialAdFb?.loadAd()
            }

            override fun onError(ad: Ad, adError: AdError) {
                initPopupAdGg()
            }
        })
        mInterstitialAdFb?.loadAd()
    }


    private fun showInterstitialAds() {
        if (!AdUtil.checkPurchaseAd(this)) {
            if (mInterstitialAdFb != null && mInterstitialAdFb!!.isAdLoaded) {
                mInterstitialAdFb?.show()
                countBackToMain = 0
                numberPopupShowed++
            } else {
                if (mInterstitialAdGg != null && mInterstitialAdGg!!.isLoaded) {
                    mInterstitialAdGg?.show()
                    countBackToMain = 0
                    numberPopupShowed++
                }
            }
        }
    }

    override fun onDestroy() {
        mInterstitialAdGg = null
        mInterstitialAdFb?.destroy()
        super.onDestroy()
    }


    override fun onBackPressed() {
        if (drawerMain.isDrawerOpen(GravityCompat.START)) {
            drawerMain.closeDrawer(GravityCompat.START)
        } else {
            val fragmentCurrent = getNavHostFragment().childFragmentManager.fragments[0]
            when (fragmentCurrent) {
                is LessonFragment -> handleBackPressInMainTab()
                else -> super.onBackPressed()
            }
        }
    }

    private fun handleBackPressInMainTab() {
        if (!Utils.checkUserRateApp(this)) {
            Utils.showDialogRate(this)
        } else {
            super.onBackPressed()
        }
    }

    private var mHelper: IabHelper? = null
    var mService: IInAppBillingService? = null

    private fun restorePurchase() {
        when {
            AdUtil.checkPurchaseAd(this@MainActivity) -> navigationMain.menu.findItem(R.id.action_remove_ads)
                .isVisible = false
            else -> {
                navigationMain.menu.findItem(R.id.action_remove_ads).isVisible = true
                val serviceIntent = Intent("com.android.vending.billing.InAppBillingService.BIND")
                serviceIntent.`package` = "com.android.vending"
                bindService(serviceIntent, mServiceConn, Context.BIND_AUTO_CREATE)
            }
        }
    }

    private var mServiceConn: ServiceConnection = object : ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName) {
            mService = null
        }

        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            mService = IInAppBillingService.Stub.asInterface(service)
            val ownedItems: Bundle
            try {
                ownedItems = mService!!.getPurchases(3, packageName, "inapp", null)
                val response = ownedItems.getInt("RESPONSE_CODE")
                if (response != 0) {
                    return
                }
                val ownedSkus = ownedItems.getStringArrayList("INAPP_PURCHASE_ITEM_LIST")
                if (ownedSkus != null) {
                    for (i in 0 until ownedSkus.size) {
                        if (ownedSkus[i] == SKU_REMOVE_ADS) {
                            successPurchaseRemoveAds()
                        }
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }

    fun successPurchaseRemoveAds() {
        if (!BuildConfig.DEBUG) {
            val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this@MainActivity)
            val editor = sharedPreferences.edit()
            editor.putBoolean(Constants.CHECK_PURCHASE, true).apply()
            val builder1 = AlertDialog.Builder(this@MainActivity)
            builder1.setTitle(getString(R.string.title_remove_ads))
            builder1.setMessage(getString(R.string.mess_remove_ads))
            builder1.setCancelable(false)
            builder1.setPositiveButton(getString(R.string.txt_ok)) { _, _ -> finish() }
            val alert11 = builder1.create()
            alert11.show()
        }
    }

    private fun startPurchase() {
        mHelper = IabHelper(this@MainActivity, base64EncodedPublicKey)
        mHelper?.startSetup(this@MainActivity)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)


        if (mHelper != null) {
            if (!mHelper!!.handleActivityResult(requestCode, resultCode, data)) {
                super.onActivityResult(requestCode, resultCode, data)
            }
        }
    }

    override fun onIabSetupFinished(result: IabResult) {
        if (result.isSuccess) {
            try {
                mHelper?.launchPurchaseFlow(
                    this@MainActivity,
                    SKU_REMOVE_ADS,
                    10001,
                    this@MainActivity
                )
            } catch (e: IabHelper.IabAsyncInProgressException) {
                e.printStackTrace()
            }
        }
    }

    override fun onIabPurchaseFinished(result: IabResult, purchase: Purchase?) {
        if (mHelper == null) {
            return
        }
        val responseCode: Int = result.response
        if (result.isFailure && responseCode != 7) {
            return
        }
        if (responseCode == 7 || purchase!!.sku == SKU_REMOVE_ADS) {
            successPurchaseRemoveAds()
        }
    }


}
