package minhphu.english.phrasalverb.data.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import minhphu.english.phrasalverb.data.database.entities.Lesson
import minhphu.english.phrasalverb.data.database.repository.LessonRepository

class LessonViewModel(lessonRepository: LessonRepository) : ViewModel() {

    var mListLesson: LiveData<List<Lesson>> = lessonRepository.getListLesson()

}